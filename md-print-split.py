#!/usr/bin/python

# A MD Python script to auto-split a set transactions
  
# from com.infinitekind.moneydance.model import *
from com.infinitekind.moneydance.model import ParentTxn, SplitTxn, AbstractTxn

root = moneydance.getRootAccount()
book = moneydance.getCurrentAccountBook()

print 'root=', root
print 'book=', book

## MAIN

# iterate through all the transactions
for txn in book.getTransactionSet().iterator():
  if txn.getParentTxn() != txn:
    continue

  print '#'
  print 'txn=', txn
  for splitNum in range(0, txn.getSplitCount()):
    splitTxn = txn.getSplit(splitNum)
    print '  splitTxn=', splitTxn

