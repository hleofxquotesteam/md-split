#!/usr/bin/python

# A MD Python script to auto-split a set transactions
  
# from com.infinitekind.moneydance.model import *
from com.infinitekind.moneydance.model import ParentTxn, SplitTxn, AbstractTxn

root = moneydance.getRootAccount()
book = moneydance.getCurrentAccountBook()

print 'root=', root
print 'book=', book

###
# Function to filter transactions
# Return True for transaction that we will accept
def accept_transaction(txn):
  # only look at the top-level transaction
  if txn.getParentTxn() != txn:
    return False

  # print 'txn.getSplitCount', txn.getSplitCount()
  # only if transaction has no split yet
  if txn.getSplitCount() != 1:
    return False

  return True

###
# Function to perform the split
def do_split(txn, fee, feeCategory):
  # Sanity check, make sure we only operate on
  # transaction that has NO split yet
  if (txn.getSplitCount() != 1):
    return False

  print '#'
  #print txn
  val = txn.getValue()
  print '  val=', val

  #for splitNum in range(0, txn.getSplitCount()):
  #  splitTxn = txn.getSplit(splitNum)
  #  print '  splitTxn=', splitNum, splitTxn

  splitTxn = txn.getSplit(0)
  print '  splitTxn.0=', splitTxn

  # TODO: hard-code value
  # new split description
  desc = 'Fee'

  # Create new split
  # public static SplitTxn makeSplitTxn(ParentTxn parentTxn, long parentAmount, long splitAmount, double rate, Account account, String description, long txnId, byte status) {
  # TODO: we are using the parent category for now (for the new split)
  if feeCategory == None:
    feeCategory = splitTxn.getAccount()
  newTxnSplit = SplitTxn.makeSplitTxn(txn, fee, fee, 1.0, feeCategory, desc, -1, splitTxn.getStatus())
  print newTxnSplit

  txn.addSplit(newTxnSplit)
  newTxnSplit.syncItem()

  # don't sync here because were are iterating in outside loop
  #txn.syncItem()
  return True

## MAIN

# TODO: hard-code value
# 0.39 * 100 = 39
# Internally, MD expresses 0.39 as 39
fee = 39

# TODO: hard-code value
feeCategory = book.getRootAccount().getAccountByName("Bank Fee")
print 'feeCategory=', feeCategory

# iterate through all the transactions
txns = []
for txn in book.getTransactionSet().iterator():
  if not accept_transaction(txn):
    continue

  if do_split(txn, fee, feeCategory):
    txns.append(txn)

# Save all the transacation that has changed
for txn in txns:
  print txn
  txn.syncItem()
